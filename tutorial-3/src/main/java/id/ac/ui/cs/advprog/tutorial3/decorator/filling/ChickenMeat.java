package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;
    String description;

    public ChickenMeat(Food food) {
        //TODO Implement
        this.food = food;
        description = ", adding chicken meat";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 4.5;
    }
}
