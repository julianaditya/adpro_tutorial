package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class CrustySandwich extends Food {
    String description;
    public CrustySandwich() {
        //TODO Implement
        description = "Crusty Sandwich";
    }

    @Override
    public double cost() {
        //TODO Implement
        return 1.0;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
