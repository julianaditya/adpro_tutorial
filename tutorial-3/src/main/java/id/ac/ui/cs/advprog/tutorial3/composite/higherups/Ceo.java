package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    String name;
    double salary;

    public Ceo(String name, double salary) {
        //TODO Implement
        this.name = name;
        if(salary < 200000.00) throw new IllegalArgumentException("salary too low");
        else this.salary = salary;
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return "CEO";
    }
}
