package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;
    String description;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
        description = ", adding cucumber";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 0.4;
    }
}
