package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    String name;
    String role;
    double salary;
    public FrontendProgrammer(String name, double salary) {
        //TODO Implement
        this.name = name;
        if(salary < 30000.00) throw new IllegalArgumentException("salary too low");
        else this.salary = salary;
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }
}