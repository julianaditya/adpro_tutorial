package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class NoCrustSandwich extends Food {
    String description;
    public NoCrustSandwich() {
        //TODO Implement
        description = "No Crust Sandwich";
    }

    @Override
    public double cost() {
        //TODO Implement
        return 2.0;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
