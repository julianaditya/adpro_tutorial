package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;
    String description;

    public TomatoSauce(Food food) {
        //TODO Implement
        this.food = food;
        description = ", adding tomato sauce";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 0.2;
    }
}
