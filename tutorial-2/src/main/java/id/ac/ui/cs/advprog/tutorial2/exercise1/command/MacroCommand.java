package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;


    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        ListIterator<Command> iterator = commands.listIterator();
        //commands.forEach(command -> command.execute());
        while(iterator.hasNext()){
            iterator.next().execute();
        }
    }

    @Override
    public void undo() {
        // TODO Complete me!

        //commands.forEach(command -> command.undo());
        for(int x = commands.size()-1;x>=0;x--){
            commands.get(x).undo();
        }
        //ListIterator<Command> iterator = commands.listIterator();
        //while(iterator.hasPrevious()) iterator.previous().undo();
    }
}
