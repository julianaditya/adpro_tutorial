package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BologneseSauceTest {
    private BologneseSauce bologneseSauce;

    @Before
    public void setUp() {
        bologneseSauce = new BologneseSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Bolognese Sauce", bologneseSauce.toString());
    }
}
