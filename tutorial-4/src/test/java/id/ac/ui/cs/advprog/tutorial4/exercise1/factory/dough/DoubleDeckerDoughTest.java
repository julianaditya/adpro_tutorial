package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DoubleDeckerDoughTest {
    private DoubleDeckerDough doubleDeckerDough;

    @Before
    public void setUp() {
        doubleDeckerDough = new DoubleDeckerDough();
    }

    @Test
    public void testToString() {
        assertEquals("Double decker dough", doubleDeckerDough.toString());
    }
}
