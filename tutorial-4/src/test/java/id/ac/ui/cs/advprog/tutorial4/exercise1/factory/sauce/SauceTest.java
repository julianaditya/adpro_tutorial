package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class SauceTest {

    private Class<?> sauceClass;

    @Before
    public void setUp() throws Exception {
        sauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.sauce.Sauce");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = sauceClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testSauceHasToStringAbstractMethod() throws Exception {
        Method toString = sauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}