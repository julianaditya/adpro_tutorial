package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class ClamPizzaTest {

    private Class<?> clamPizzaClass;

    @Before
    public void setUp() throws Exception {
        clamPizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "pizza.ClamPizza");
    }

    @Test
    public void testClamPizzaIsAPizza() {
        Class<?> parent = clamPizzaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "pizza.Pizza", parent.getName());
    }

    @Test
    public void testClamPizzaOverridePrepareMethod() throws Exception {
        Method prepare = clamPizzaClass.getDeclaredMethod("prepare");
        int methodModifiers = prepare.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", prepare.getGenericReturnType().getTypeName());
    }
}