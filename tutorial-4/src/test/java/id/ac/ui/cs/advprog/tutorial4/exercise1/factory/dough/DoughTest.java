package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class DoughTest {

    private Class<?> doughClass;

    @Before
    public void setUp() throws Exception {
        doughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.dough.Dough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = doughClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testDoughHasToStringAbstractMethod() throws Exception {
        Method toString = doughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}