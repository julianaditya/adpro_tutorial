package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class VeggiesTest {

    private Class<?> veggiesClass;

    @Before
    public void setUp() throws Exception {
        veggiesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.veggies.Veggies");
    }

    @Test
    public void testVeggiesIsAPublicInterface() {
        int classModifiers = veggiesClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testVeggiesHasToStringAbstractMethod() throws Exception {
        Method toString = veggiesClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}