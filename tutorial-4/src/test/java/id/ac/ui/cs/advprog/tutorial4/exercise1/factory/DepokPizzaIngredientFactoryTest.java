package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokPizzaIngredientFactoryTest {
    PizzaIngredientFactory ingredientFactory =
            new DepokPizzaIngredientFactory();

    @Test
    public void createDoughTest() {
        assertEquals("Double decker dough",  ingredientFactory.createDough().toString());
    }

    @Test
    public void createSauceTest() {
        assertEquals("Bolognese Sauce",  ingredientFactory.createSauce().toString());
    }

    @Test
    public void createCheeseTest() {
        assertEquals("Slices of cheddar cheese",  ingredientFactory.createCheese().toString());
    }

    @Test
    public void createVeggiesTest() {
        String[] test = {"Pineapple", "Spinach", "Black Olives", "Eggplant"};
        Veggies[] veggies = ingredientFactory.createVeggies();
        for (int x=0;x<veggies.length;x++){
            assertEquals(test[x],veggies[x].toString());
        }
    }

    @Test
    public void createClamTest() {
        assertEquals("Scallops straight from the harbor",  ingredientFactory.createClam().toString());
    }
}