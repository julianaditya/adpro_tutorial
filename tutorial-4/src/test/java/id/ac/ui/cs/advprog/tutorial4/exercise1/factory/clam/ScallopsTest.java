package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ScallopsTest {
    private Scallops scallops;

    @Before
    public void setUp() {
        scallops = new Scallops();
    }

    @Test
    public void testToString() {
        assertEquals("Scallops straight from the harbor", scallops.toString());
    }
}
