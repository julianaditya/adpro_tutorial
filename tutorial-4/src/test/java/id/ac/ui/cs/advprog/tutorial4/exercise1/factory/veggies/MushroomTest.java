package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MushroomTest {
    private Mushroom mushroom;

    @Before
    public void setUp() {
        mushroom = new Mushroom();
    }

    @Test
    public void testToString() {
        assertEquals("Mushrooms", mushroom.toString());
    }
}
