package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PineappleTest {
    private Pineapple pineapple;

    @Before
    public void setUp() {
        pineapple = new Pineapple();
    }

    @Test
    public void testToString() {
        assertEquals("Pineapple", pineapple.toString());
    }
}
