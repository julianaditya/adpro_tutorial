package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokPizzaStoreTest {
    PizzaStore dStore = new DepokPizzaStore();
    @Test
    public void createPizza() {
        Pizza pizza = dStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza",pizza.getName());
        pizza = dStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza",pizza.getName());
        pizza = dStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza",pizza.getName());
    }
}