package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private static Singleton singleton;

    private Singleton(){}

    /*
    the difference between lazy and eager implementation is the instantiation of the class.
    lazy implementation only instantiate when needed.
     */
    public static Singleton getInstance() {
        // TODO Implement me!
        // lazy implementation
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }
    /*
    public static Singleton getInstance() {
        // TODO Implement me!
        // eager implementation
        return singleton;
    }
     */
}
