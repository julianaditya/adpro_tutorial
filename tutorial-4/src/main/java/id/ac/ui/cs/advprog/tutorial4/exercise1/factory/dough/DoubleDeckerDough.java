package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class DoubleDeckerDough implements Dough {
    public String toString() {
        return "Double decker dough";
    }
}
