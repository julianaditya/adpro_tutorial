package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class Scallops implements Clams {

    public String toString() {
        return "Scallops straight from the harbor";
    }
}
